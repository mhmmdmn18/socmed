<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\User;

class UserController extends Controller
{
    public function index() {
        $users = User::all()->except(['id' => Auth::user()->id]);
        return view('users.index', compact('users'));
    }

    /**
    * Show the user details page.
    * @param int $userId 
    */
    public function show(int $userId){
        $user = User::find($userId);
        $followers = $user->followers;
        $followings = $user->followings;

        return view('users.show', compact('user', 'followers' , 'followings'));
    }
}
