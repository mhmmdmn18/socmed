@extends('layouts.app')

@section('content')

<div class="form-group">
    <label>Name</label>
    <input type="text" name="name" value="{{ auth()->profiles()->name }}" required form="form-update-profile" />
</div>
 
<div class="form-group">
    <label>Website</label>
    <input type="text" name="website"value="{{ auth()->profiles()->website }}" form="form-update-profile" autocomplete="off" />
</div>
 
 
<div class="form-group">
    <label>Bio</label>
    <input type="text" name="bio" value="{{ auth()->profiles()->bio }}" form="form-update-profile" autocomplete="off" />
</div>

<div class="form-group row">
    <label for="photo" class="col-md-4 col-form-label text-md-right">Profile Image</label>
    <div class="col-md-6">
        <input id="photo" type="file" class="form-control" name="photo">
        @if (auth()->profiles()->photo)
            <code>{{ auth()->profiles()->photo }}</code>
        @endif
    </div>

    
@endsection 