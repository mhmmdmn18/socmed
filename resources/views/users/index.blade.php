@extends('layouts.app')

@section('content')
	@foreach ($users as $user)
	<div class="container">
	<table>
		<tr>
			<td class="w-100"><a href="{{ route('user.show', $user->id) }}">{{ $user->username }}</a></td>
			<td><a href="{{ route('user.follow', $user->id) }}" class="btn btn-primary">Follow</a></button></td>
			<td><a href="{{ route('user.unfollow', $user->id) }}" class="btn btn-danger">Unfollow</a></td>
		</tr>
		<tr>
			<td><hr></td>
			<td><hr></td>
			<td><hr></td>
		</tr>
	</table>
	</div>
	@endforeach
@endsection