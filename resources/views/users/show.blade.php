@extends('layouts.app')

@section('content')
	<div class="container">
		<h3>{{ $user->username }}</h3>

		<br><span>Followers ({{count($followers)}}) :</span><br>
		@foreach($followers as $follower)
		{{ $follower->username }}
		<br>
		@endforeach

		<br><span>Followings ({{count($followings)}}) :</span><br>
		@foreach($followings as $following)
		{{ $following->username }}
		<br>
		@endforeach
	</div>
@endsection