<?php



Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/profile', 'HomeController@profile');
Route::get('/users', 'UserController@index')->name('users');
Route::get('/user/{userId}', 'UserController@show')->name('user.show');

Route::get('/{profileId}/follow', 'ProfileController@followUser')->name('user.follow');
Route::get('/{profileId}/unfollow', 'ProfileController@unFollowUser')->name('user.unfollow');

